(define-library (ch1Tests)
  (import (scheme base)
          (scheme write)
          (srfi 78)
          (ch1))
  (export run-tests-ch1)  
  (begin
    (define run-tests-ch1
      (lambda ()
        (test-atom?)))
    (define test-atom?
      (lambda ()
        (check (atom? (quote ())) => #f)))))
