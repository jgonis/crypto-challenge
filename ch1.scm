(define-library (ch1)
  (import (scheme base)
          (scheme char))
  (export caesar-shift
          char->alphabetic-value
          alphabetic-value->char
          char-shift-value)
  (begin
    (define A-CHAR-VALUE (char->integer #\a))
    
    (define (char->alphabetic-value char)
      (let* ((lower-char (char-downcase char))
             (lower-char-number (char->integer lower-char))
             (char-alphabetic-number (- lower-char-number
                                        A-CHAR-VALUE)))
        char-alphabetic-number))
    
    (define (alphabetic-value->char alphabetic-value)
      (let* ((char-value (+ alphabetic-value A-CHAR-VALUE))
             (char (integer->char char-value)))
        char))

    (define (char-shift-value char1 char2)
      (let ((char1-value (char->alphabetic-value char1))
            (char2-value (char->alphabetic-value char2)))
        (modulo (- char2-value char1-value) 26)))
    
    (define (caesar-shift cipher-text shift-amount)
      (define (helper cipher-text shift-amount idx deciphered-text)
        (cond ((= idx (vector-length cipher-text))
               (vector->string deciphered-text))
              
              (else
               (let* ((cipher-char (vector-ref cipher-text idx))
                     (deciphered-char cipher-char))
                 (cond ((char-alphabetic? cipher-char)
                        (let* ((char-number (char->alphabetic-value
                                             cipher-char))
                               (shifted-char-number (modulo (+ shift-amount
                                                               char-number)
                                                            26))
                               (shifted-char (alphabetic-value->char
                                              shifted-char-number)))
                          (set! deciphered-char shifted-char))))
                 
                 (vector-set! deciphered-text idx deciphered-char)
                 (helper cipher-text
                         shift-amount
                         (+ idx 1)
                         deciphered-text)))))
      (let ((cipher-string-vec (string->vector cipher-text)))
        (helper cipher-string-vec
                shift-amount
                0
                (make-vector (vector-length cipher-string-vec)))))))
